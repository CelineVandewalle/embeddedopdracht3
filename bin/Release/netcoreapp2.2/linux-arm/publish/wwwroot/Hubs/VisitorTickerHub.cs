using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SignalR.Hubs
{

    public class VisitorTickerHub : Hub
    {
        public Task SendRoom1()
            {
                var client = new MongoClient("mongodb://localhost:27017");
                var database = client.GetDatabase("visitors");
                var room1coll = database.GetCollection<BsonDocument>("room1");
                var room1count = room1coll.CountDocuments(new BsonDocument());
                return Clients.All.SendAsync("Room1Count",room1count);
            }
            public Task SendRoom2()
            {
                var client = new MongoClient("mongodb://localhost:27017");
                var database = client.GetDatabase("visitors");
                var room2coll = database.GetCollection<BsonDocument>("room2");
                var room2count = room2coll.CountDocuments(new BsonDocument());
                return Clients.All.SendAsync("Room2Count",room2count);
            }

            public Task SendLatestVisitor()
            {
                return Clients.All.SendAsync("LatestVisitor",@SignalR.Startup.lastVisit);
            }
    }
}