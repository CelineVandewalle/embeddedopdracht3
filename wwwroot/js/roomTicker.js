"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/VisitorTickerHub").build();
//Room1Count Room2Count LatestVisitor
connection.on("Room1Count", function ( message) {
    document.getElementById("room1counter").textContent=message;
});

connection.on("Room2Count", function ( message) {
    document.getElementById("room2counter").textContent=message;
});

connection.on("LatestVisitor", function ( message) {
    document.getElementById("latestVisitor").textContent=message;
});


connection.start().catch(function (err) {
    return console.error(err.toString());
});

