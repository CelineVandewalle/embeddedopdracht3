using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SignalR.Models
{
    public class Visitor
    {
        public ObjectId Id { get; set; }

        [BsonElement("date")]
        public string date { get; set; }

        [BsonElement("time")]
        public string time { get; set; }

        [BsonElement("room")]
        public string room { get; set; }
    }
}