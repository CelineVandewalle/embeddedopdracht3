
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR.Client;
using SignalR.Hubs;
using MongoDB.Driver;
using MongoDB.Bson;

namespace SignalR
{
    public class tickerWorker
    {
       HubConnection connection;
       bool connectionReady = false;
       public tickerWorker()
       {
          Init();
       }
        public async void Init()
        {
            await Task.Delay(10000); //Wait 10 s that the server starts
            await InitHub();
            connectionReady = true;
        }

        public async Task<bool> InitHub()
        {
             try
             {
                connection = new HubConnectionBuilder()
                    .WithUrl("http://localhost:5000/VisitorTickerHub/")
                    .Build();
                await connection.StartAsync();
                await connection.StartAsync();
                  
                Console.WriteLine("Hub connection started");
                
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
                return false;
            }
        }

        public async void sendInfo()
        {   
            if (connectionReady)
            {
                var client = new MongoClient("mongodb://localhost:27017");
                var database = client.GetDatabase("visitors");
                var room2coll = database.GetCollection<BsonDocument>("room2");
                var room2count = room2coll.CountDocuments(new BsonDocument());
                var room1coll = database.GetCollection<BsonDocument>("room1");
                var room1count = room1coll.CountDocuments(new BsonDocument());
               // await connection.InvokeAsync("SendMessage", "Room2Count",room2count.ToString());
               // await connection.InvokeAsync("SendMessage", "Room1Count",room1count.ToString());
              //  await connection.InvokeAsync("SendMessage","LatestVisitor",@SignalR.Startup.lastVisit.ToString());
                }
                
        }
    }

}