﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
// using Unosquare.RaspberryIO;
// using Unosquare.RaspberryIO.Gpio;

namespace SignalR.Pages
{
    public class doorControlsModel : PageModel
    {
 
            
        public void OnGet()
        {

        }
   
        public void OnPost()
        {   
            
            string room = "room" +Request.Form["roomNr"];
            var client = new MongoDB.Driver.MongoClient ("mongodb://localhost:27017");
            var database = client.GetDatabase("visitors");
            var roomcoll = database.GetCollection<MongoDB.Bson.BsonDocument>(room);
            var document = new MongoDB.Bson.BsonDocument
            {
                { "date", DateTime.Now.ToShortDateString() },
                { "time", DateTime.Now.ToShortTimeString()},
                { "room", room }
            };
            @SignalR.Startup.lastVisit = DateTime.Now.ToLongTimeString();
            roomcoll.InsertOne(document);
            //             if (room == "room1")
            // {
            //     var pin = Pi.Gpio.Pin05;
            //     pin.PinMode = GpioPinDriveMode.Output;
            //     pin.Write(true);
            //     Thread.Sleep(250);
            //     pin.Write(false);
            // }
            // if (room == "room2")
            // {
            //     var pin = Pi.Gpio.Pin06;
            //     pin.PinMode = GpioPinDriveMode.Output;
            //     pin.Write(true);
            //     Thread.Sleep(250);
            //     pin.Write(false);
            // }
        }
        
    }
}