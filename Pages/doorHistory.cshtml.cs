﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MongoDB.Bson;
using MongoDB.Driver;
using SignalR.Models;
using SignalR.Services;
using System.Data;
using System.ComponentModel;

namespace SignalR.Pages
{
    public class doorHistoryModel : PageModel
    {
        public List<Visitor> room1  { get; set; }
        public List<Visitor> room2 { get; set; }
        public void OnGet()
        {
            visitorService service = new visitorService();
            room1 = service.GetRoom1();
            room2 = service.GetRoom2();
        }
    }
}