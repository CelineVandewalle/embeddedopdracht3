using System.Collections.Generic;
using System.Linq;
using SignalR.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SignalR.Services
{
    public class visitorService
    {
        private readonly IMongoCollection<Visitor> _room1,_room2;

        public visitorService()
        {
            var client = new MongoDB.Driver.MongoClient ("mongodb://localhost:27017");
            var database = client.GetDatabase("visitors");
            
            _room1 = database.GetCollection<Visitor>("room1");
            _room2 = database.GetCollection<Visitor>("room2");
        }

        public List<Visitor> GetRoom1()
        {
            return _room1.Find(Visitor => true).ToList();
        }
        public List<Visitor> GetRoom2()
        {
            return _room1.Find(Visitor => true).ToList();
        }
    }
}