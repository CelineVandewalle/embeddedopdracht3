﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginScreen.aspx.cs" Inherits="WebApp.LoginScreen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="style.css" type="text/css"/>
    <title>the fun page</title>
</head>
<body>
    <h1>Create an account</h1>
    <form id="form1" runat="server">
        <div>
            <label for="username"><b>Username</b></label>
            <input type="text" placeholder="Choose your username" name="username" required="required" /><br />
            <label for="password"><b>Password</b></label>
            <input type="password" placeholder="Choose Password" name="password" required="required"/>
            <br />
            <button type="submit">Create account</button>
        </div>
    </form>
</body>
</html>
