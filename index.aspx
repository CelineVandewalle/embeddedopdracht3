﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApp.demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <link rel="stylesheet" href="style.css" type="text/css"/>
    <title>the fun page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label for="username"><b>Username</b></label>
            <input type="text" placeholder="Enter your username" name="username" required="required" /><br />
            <label for="password"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required="required"/>
            <br />
            <button type="submit">Login</button>
            <div>If you don't have an account, create one <a href="LoginScreen.aspx">here</a>
            </div>
        </div>
    </form>
</body>
</html>
